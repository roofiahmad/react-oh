import "./App.css";
import Page from "./components/Page";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
function App() {
  return (
    <Router>
      <div>
        <nav>
          <Link to="/page">Page</Link>
          <Link to="/">Home</Link>
        </nav>
        <Switch>
          <Route path="/page">
            <Page />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function Home() {
  return <h2>You're in home</h2>;
}

export default App;
