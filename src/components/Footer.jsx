import React from 'react'
import './footer.css'
import {connect} from 'react-redux'

const Footer = (props) => {
    console.log(props);
    return (
        <div className="bg-dark footer">
            <p>copyright@{props.name} 2020</p>
            <ul>
                <li>instagram</li>
                <li>twitter</li>
                <li>facebook</li>
            </ul>
        </div>
    )
}

function mapStateToProps(state){
    return {name: state.name}
};
export default connect(mapStateToProps)(Footer)