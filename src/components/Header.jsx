import React from 'react'
import { Navbar, Nav, Form,Button, FormControl } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {getName} from './redux/action/action'
import {store} from './redux/store/store'

export default function Header() {
    return (
        <div>
        <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">Navbar</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link href="#home">Home</Nav.Link>
          <Nav.Link href="#features">Features</Nav.Link>
          <Nav.Link href="#pricing">Pricing</Nav.Link>
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="outline-info" onClick= {() => getName(store)}>Search</Button>
        </Form>
      </Navbar>
        </div>
    )
}
