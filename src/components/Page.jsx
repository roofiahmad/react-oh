import React from 'react'
import Header from './Header'
import Footer from './Footer'
import {Provider} from 'react-redux'
import {store} from './redux/store/store'


export default function Page() {
    return (
        <div>
            <Header/>
            <br/>
            <br/>
            <Provider store = {store}>
            <Footer/>
            </Provider>
        </div>
    )
}
