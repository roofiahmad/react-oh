const initialState = { name: "React" };
export default (state = initialState, action) => {
  switch (action.type) {
    case "GET_NAME":
      return { ...state, name: "Roofi" };
    default:
      return state;
  }
};
